#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>

using namespace std;
void main()
{
	srand(time(NULL));
	cout << "Enter size of array: ";
	int size;
	cin >> size;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}

	while (true)
	{
		cout << "\nGenerated array: " << endl;
		cout << "Unordered: ";
		for (int i = 0; i < unordered.getSize(); i++)
			cout << unordered[i] << "   ";
		cout << "\nOrdered: ";
		for (int i = 0; i < ordered.getSize(); i++)
			cout << ordered[i] << "   ";
		char playerInput;
		cout << endl << "What do you want to do?" << endl
			<< "1 - Remove an element at index" << endl
			<< "2 - Search for an element" << endl
			<< "3 - Expand and generate random values" << endl;
		cin >> playerInput;
		int result;
		switch (playerInput)
		{
		case '1':
			int index;
			cout << "Index of number to be removed:";
			cin >> index;

			unordered.remove(index);
			ordered.remove(index);
			break;
		case '2':
			cout << "\nEnter number to search: ";
			int input;
			cin >> input;
			cout << endl;

			cout << "Unordered Array(Linear Search):\n";
			result = unordered.linearSearch(input);
			if (result >= 0)
				cout << input << " was found at index  " << result << endl;
			else
				cout << input << " not found." << endl;

			cout << "\nOrdered Array(Binary Search):\n";
			result = ordered.binarySearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << endl;
			else
				cout << input << " not found." << endl;
			system("pause");
			system("cls");
			break;
		case '3':
			cout << "Enter amount to expand by: ";
			cin >> size;
			for (int i = 0; i < size; i++)
			{
				int rng = rand() % 101;
				unordered.push(rng);
				ordered.push(rng);
			}
			break;
		}
	}
}