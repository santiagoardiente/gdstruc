#include <iostream>
#include <stdio.h>
using namespace std;

int digitSum(int input)
{
	// int sum = 0;
	if (input == 0)
		return 0;

	if (input == 1)
		cout << input % 10 << " = "; // if last value
	else 
		cout << input % 10 << " + ";

	//sum = input % 10;
	// digitSum(input / 10);
	// cout << sum + digitSum(input / 10);
	return input % 10 + digitSum(input / 10);
}

void printFibo(int input, int current = 1, int previous = 0)
{
	if (input == 0)
		return;
	cout << current << " ";
	printFibo(input - 1, current + previous, current);
}
void isPrimeNum(int input, int x = 2)
{
	if (input <= 2)
	{
		cout << "It is a Prime Number";
		return;
	}
	else if (input % x == 0)
	{
		cout << "It is NOT a Prime Number";
		return;
	}
	else if (x * x > input)
	{
		cout << "It is a Prime Number";
		return;
	}
	isPrimeNum(input, x + 1);
}

int main()
{
	int input;
	
	// Sum of Digits
	cout << "Sum of Digits " << endl << "Enter number: ";
	cin >> input;
	cout << digitSum(input);
	// cout << digitSum(12345);

	cout << endl << endl;

	// Fibonacci
	cout << "Fibonacci " << endl << "Enter nth value: ";
	cin >> input;
	printFibo(input);
	//printFibo(10);

	cout << endl << endl;

	// Prime Number
	cout << "Prime Number Checker " << endl << "Enter a number: ";
	 cin >> input;
	 isPrimeNum(input);
	// isPrimeNum(10);


	cout << endl;
	system("pause");
	return 0;
}