#pragma once
#include "UnorderedArray.h"
#include <assert.h>
#include <cstring>
#include <iostream>
template <class T>

class Queue : public UnorderedArray<T>
{
public:
	Queue(int size) : UnorderedArray<T>(size) {}
private:
	UnorderedArray<T>* mContainer;
};
