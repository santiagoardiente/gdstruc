#include "Stack.h"
#include "Queue.h"
#include "Queue.h"
#include <iostream>

using namespace std;

int main()
{
	int size, userChoice;

	cout << "Enter size for Element sets: ";
	cin >> size;
	Queue<int> mQueue(size);
	Stack<int> mStack(size);
	
	while(true)
	{
		cout << "What do you want to do?" << endl
			<< "[1] - Push Elements" << endl
			<< "[2] - Pop Elements" << endl
			<< "[3] - Print everything then empty sets" << endl;
		cin >> userChoice;

		int input;
		switch (userChoice)
		{
		case 1:
			cout << "Enter element to be pushed: ";
			cin >> input;

			mQueue.push(input);
			cout << "Queue Top Element: " << mQueue.display(0) << endl;

			mStack.push(input);
			cout << "Stack Top Element " << mStack.display(mStack.getSize() - 1) << endl;

			break;
		case 2:
			cout << "Top elements have been removed" << endl
				<< "Queue  Top Element removed: ";
			mQueue.popFront(); 
			cout << endl << "Stack Top Element removed: ";
			mStack.pop();

			cout << endl << endl << "New Top Elements:" << endl 
				<< "Queue: " << mQueue.display(0) << endl 
				<< "Stack: " << mStack.display(mStack.getSize() - 1);

			break;
		case 3:
			cout << "Queue : ";
			for (int x = 0; x < mQueue.getSize(); x++)
				cout << mQueue.display(x) << " ";
			cout << endl << "Stack : ";
			for (int x = 0; x < mStack.getSize(); x++)
				cout << mStack.display(x) << " ";
			mQueue.clearArray();
			mStack.clearArray();
			break;
		}
		cout << endl;
		system("pause");
		system("cls");
	}

	return 0;
}