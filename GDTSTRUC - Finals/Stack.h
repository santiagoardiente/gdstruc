#pragma once
#include "UnorderedArray.h"
#include <assert.h>
#include <cstring>
#include <iostream>
template <class T>

class Stack : public UnorderedArray<T>
{
public:
	Stack(int size) : UnorderedArray<T>(size) {}
private:
	UnorderedArray<T>* mContainer;
};
